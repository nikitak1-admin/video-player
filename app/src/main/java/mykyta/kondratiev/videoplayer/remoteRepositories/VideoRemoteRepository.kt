package mykyta.kondratiev.videoplayer.remoteRepositories

import mykyta.kondratiev.videoplayer.model.CategoryResponse
import retrofit2.Response
import retrofit2.http.GET

interface VideoRemoteRepository {
    @GET("videos")
    suspend fun getAll() : Response<CategoryResponse>
}