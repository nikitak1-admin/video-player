package mykyta.kondratiev.videoplayer.components

import android.content.Context
import dagger.Component
import mykyta.kondratiev.videoplayer.interactors.GetAllVideosInteractor
import mykyta.kondratiev.videoplayer.modules.VideoModule
import javax.inject.Singleton

@Singleton
@Component(modules = [VideoModule::class])
interface VideoComponent {
    fun getGetAllVideosInteractor() : GetAllVideosInteractor
}