package mykyta.kondratiev.videoplayer.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import mykyta.kondratiev.videoplayer.model.Video

@Dao
public interface VideoDao {
    @Insert
    fun insert(video: Video)

    @Insert
    fun insertAll(videos: List<Video>)

    @Delete
    fun delete(video: Video)

    @Delete
    fun deleteAll(videos: List<Video>)

    @Query("SELECT * FROM video")
    fun getAll() : List<Video>
}