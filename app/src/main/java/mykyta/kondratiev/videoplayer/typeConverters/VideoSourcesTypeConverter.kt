package mykyta.kondratiev.videoplayer.typeConverters

import androidx.room.TypeConverter

public class VideoSourcesTypeConverter {
    @TypeConverter
    public fun stringToListSources(data: String) : List<String>{
        return listOf(data)
    }

    @TypeConverter
    public fun listSourcesToString(sources: List<String>) : String{
        return sources[0]
    }
}