package mykyta.kondratiev.videoplayer.interactors

import kotlinx.coroutines.flow.Flow
import mykyta.kondratiev.videoplayer.model.Video
import mykyta.kondratiev.videoplayer.repositories.VideoRepository

class GetAllVideosInteractor (private val videoRepository: VideoRepository){
    suspend operator fun invoke(): List<Video> {
        return videoRepository.getAllVideos()
    }
}