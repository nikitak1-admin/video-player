package mykyta.kondratiev.videoplayer.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.*
import mykyta.kondratiev.videoplayer.interactors.GetAllVideosInteractor
import mykyta.kondratiev.videoplayer.model.Video
import kotlin.coroutines.CoroutineContext

class MainViewModel constructor(private val getAllVideosInteractor: GetAllVideosInteractor) :
    ViewModel() {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val _videos = MutableLiveData<List<Video>>()
    val videos = _videos

    init {
        load()
    }

    private fun load() {
        scope.launch {
            var result : List<Video>?
            withContext(Dispatchers.IO){
                result = getAllVideosInteractor()
            }
            withContext(Dispatchers.Main){
                _videos.postValue(result!!)
            }
        }

    }
}