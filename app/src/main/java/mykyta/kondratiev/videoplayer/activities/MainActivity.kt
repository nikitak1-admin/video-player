package mykyta.kondratiev.videoplayer.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mykyta.kondratiev.videoplayer.R
import mykyta.kondratiev.videoplayer.adapters.VideoAdapter
import mykyta.kondratiev.videoplayer.components.DaggerVideoComponent
import mykyta.kondratiev.videoplayer.interactors.GetAllVideosInteractor
import mykyta.kondratiev.videoplayer.modules.VideoModule
import mykyta.kondratiev.videoplayer.viewModels.MainViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewModel: MainViewModel
    private lateinit var getAllVideosInteractor: GetAllVideosInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getAllVideosInteractor = DaggerVideoComponent.builder()
            .videoModule(VideoModule(applicationContext)).build()
            .getGetAllVideosInteractor()
        viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory{
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MainViewModel(getAllVideosInteractor) as T
            }
        }).get(MainViewModel::class.java)
        viewModel.videos.observe(this, { result ->
            recyclerView = findViewById<RecyclerView>(R.id.recycler_view).apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = VideoAdapter(result)
            }
        })
    }
}
