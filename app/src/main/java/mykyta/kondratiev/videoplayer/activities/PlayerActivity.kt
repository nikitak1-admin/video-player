package mykyta.kondratiev.videoplayer.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import kotlinx.android.synthetic.main.activity_player.*
import mykyta.kondratiev.videoplayer.*
import mykyta.kondratiev.videoplayer.components.DaggerVideoComponent
import mykyta.kondratiev.videoplayer.interactors.GetAllVideosInteractor
import mykyta.kondratiev.videoplayer.model.Video
import mykyta.kondratiev.videoplayer.modules.VideoModule
import mykyta.kondratiev.videoplayer.viewModels.PlayerViewModel

class PlayerActivity : AppCompatActivity() {
    private lateinit var viewModel: PlayerViewModel
    private lateinit var playerView: PlayerView
    private lateinit var videoPlayer: ExoPlayer
    private lateinit var dataSourceFactory: DefaultHttpDataSource.Factory
    private var position = 0
    private lateinit var getAllVideosInteractor: GetAllVideosInteractor


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        getAllVideosInteractor = DaggerVideoComponent.builder()
            .videoModule(VideoModule(applicationContext)).build()
            .getGetAllVideosInteractor()
        videoPlayer = ExoPlayer.Builder(this).build()
        dataSourceFactory = DefaultHttpDataSource.Factory()
        viewModel = ViewModelProvider(this, object : ViewModelProvider.Factory{
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return PlayerViewModel(getAllVideosInteractor) as T
            }
        }).get(PlayerViewModel::class.java)
        position = intent.getIntExtra("position", 0)
        playerView = findViewById(R.id.video_player_view)
        video_player_view.player = videoPlayer
        viewModel.videos.observe(this, { result ->
            initializePlayer(result)
        })

    }

    private fun buildMediaSource(video : Video): MediaSource {
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(
            MediaItem.fromUri(video.sources[0]))
    }

    private fun initializePlayer(videos: List<Video>) {
        playerView.player = videoPlayer
        for (i in position until videos.size){
            videoPlayer.addMediaSource(buildMediaSource(videos[i]))
        }
        for (i in 1 until position) {
            videoPlayer.addMediaSource(buildMediaSource(videos[i]))
        }
        videoPlayer.prepare()
        videoPlayer.playWhenReady = true
    }

    override fun onBackPressed() {
        videoPlayer.stop()
        super.onBackPressed()
    }

}
