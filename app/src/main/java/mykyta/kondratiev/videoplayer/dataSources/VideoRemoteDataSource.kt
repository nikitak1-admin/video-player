package mykyta.kondratiev.videoplayer.dataSources

import mykyta.kondratiev.videoplayer.model.Video
import mykyta.kondratiev.videoplayer.remoteRepositories.VideoRemoteRepository
import retrofit2.Response
import retrofit2.Retrofit

class VideoRemoteDataSource(private val retrofit: Retrofit) {

    private val videoRemoteRepository: VideoRemoteRepository =
        retrofit.create(VideoRemoteRepository::class.java)

    suspend fun getAll() : List<Video>? {
        val movieResponse = safeApiResult(
            call = { videoRemoteRepository.getAll()}
        )

        return movieResponse?.categories?.get(0)?.videos;
    }



    private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>) : T?{
        val response = call.invoke()
        if(response.isSuccessful) return response.body()
        return null
    }
}