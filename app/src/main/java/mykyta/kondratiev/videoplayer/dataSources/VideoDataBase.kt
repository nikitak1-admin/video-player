package mykyta.kondratiev.videoplayer.dataSources

import androidx.room.Database
import androidx.room.RoomDatabase
import mykyta.kondratiev.videoplayer.dao.VideoDao
import mykyta.kondratiev.videoplayer.model.Video

@Database(entities = [Video::class], version = 1)
abstract class VideoDataBase : RoomDatabase() {
    abstract fun getVideoDao(): VideoDao
}