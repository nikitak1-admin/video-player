package mykyta.kondratiev.videoplayer.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import mykyta.kondratiev.videoplayer.typeConverters.VideoSourcesTypeConverter

@Entity
@TypeConverters(VideoSourcesTypeConverter::class)
data class Video(
    @SerializedName("description" )
    val description: String,

    @SerializedName("sources" )
    val sources: List<String>,

    @SerializedName("subtitle" )
    val subtitle: String,

    @SerializedName("thumb" )
    val thumb: String,

    @PrimaryKey
    @SerializedName("title" )
    val title: String
)