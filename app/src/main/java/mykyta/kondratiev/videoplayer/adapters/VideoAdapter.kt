package mykyta.kondratiev.videoplayer.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mykyta.kondratiev.videoplayer.R
import mykyta.kondratiev.videoplayer.activities.PlayerActivity
import mykyta.kondratiev.videoplayer.model.Video

class VideoAdapter(private val videos: List<Video>) : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_video, parent, false)
        )

    override fun getItemCount(): Int = videos.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val video = videos[position]

        holder.titleTextView.text = video.title
        holder.subtitleTextView.text = video.subtitle
        holder.descriptionTextView.text = video.description

        holder.view.setOnClickListener {
            val intent = Intent(it.context, PlayerActivity::class.java)
            intent.putExtra("position", position)
            it.context.startActivity(intent)
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView: TextView = view.findViewById(R.id.title_text)
        val subtitleTextView: TextView = view.findViewById(R.id.subtitle_text)
        val descriptionTextView: TextView = view.findViewById(R.id.desc_text)
    }
}