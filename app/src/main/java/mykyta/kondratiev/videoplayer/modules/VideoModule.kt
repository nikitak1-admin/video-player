package mykyta.kondratiev.videoplayer.modules

import android.content.Context
import androidx.room.Room
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import mykyta.kondratiev.videoplayer.dao.VideoDao
import mykyta.kondratiev.videoplayer.dataSources.VideoDataBase
import mykyta.kondratiev.videoplayer.dataSources.VideoRemoteDataSource
import mykyta.kondratiev.videoplayer.interactors.GetAllVideosInteractor
import mykyta.kondratiev.videoplayer.repositories.VideoRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class VideoModule(private val context: Context) {

    @Provides
    fun provideContext(): Context {
        return context
    }

    @Provides
    fun provideGetAllVideosInteractor(videoRepository: VideoRepository) : GetAllVideosInteractor {
        return GetAllVideosInteractor(videoRepository)
    }

    @Provides
    fun provideVideoRepository(videoRemoteDataSource: VideoRemoteDataSource, videoDao: VideoDao) :
            VideoRepository {
        return VideoRepository(videoRemoteDataSource, videoDao)
    }

    @Provides
    fun provideVideoRemoteDataSource(retrofit: Retrofit) : VideoRemoteDataSource {
        return VideoRemoteDataSource(retrofit)
    }

    @Provides
    fun provideMovieDao(videoDatabase: VideoDataBase): VideoDao {
        return videoDatabase.getVideoDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): VideoDataBase {
        return Room.databaseBuilder(
            context,
            VideoDataBase::class.java,
            "videoDB.db"
        ).build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder()
            .baseUrl("https://raw.githubusercontent.com/DavidStdn/NitrixTestTask/main/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }
}