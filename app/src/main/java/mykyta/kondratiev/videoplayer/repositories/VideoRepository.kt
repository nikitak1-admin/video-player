package mykyta.kondratiev.videoplayer.repositories

import mykyta.kondratiev.videoplayer.dao.VideoDao
import mykyta.kondratiev.videoplayer.dataSources.VideoRemoteDataSource
import mykyta.kondratiev.videoplayer.model.Video

class VideoRepository(private val videoRemoteDataSource: VideoRemoteDataSource,
                      private val videoDao: VideoDao) {
    suspend fun getAllVideos(): List<Video> {
        val result = videoRemoteDataSource.getAll().let {
            videoDao.deleteAll(it!!)
            videoDao.insertAll(it)
        }
        return videoDao.getAll()
    }
}